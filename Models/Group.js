const mongoose = require("mongoose");

const groupSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  owner: {
    type: mongoose.Types.ObjectId,
    ref: "user",
  },
  members: [
    {
      type: mongoose.Types.ObjectId,
      ref: "user",
    },
  ],
  items: [
    {
      type: mongoose.Types.ObjectId,
      ref: "item",
    },
  ],
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = Group = mongoose.model("group", groupSchema);
