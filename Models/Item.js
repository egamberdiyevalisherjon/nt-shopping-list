const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  owner: {
    type: mongoose.Types.ObjectId,
    ref: "user",
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  boughtAt: Date,
  boughtBy: {
    type: mongoose.Types.ObjectId,
    ref: "user",
  },
  isBought: {
    type: Boolean,
    required: true,
    default: false,
  },
  group: {
    type: mongoose.Types.ObjectId,
    ref: "group",
  },
});

module.exports = Item = mongoose.model("item", itemSchema);
