const express = require("express");
const cors = require("cors");

const authRoute = require("./Routes/Auth");
const userRoute = require("./Routes/User");
const groupRoute = require("./Routes/Group");
const itemRoute = require("./Routes/Item");

const app = express();

app.use((req, res, next) => {
  console.log(req.method, req.url);
  if (req.url.slice(-1) === "/" && req.path.length > 1) {
    req.url = req.url.slice(0, -1);
  }
  console.log(req.method, req.url);
  next();
});

app.use(express.json());

app.use(
  cors({
    origin: "*",
  })
);

app.use("/api/auth", authRoute);
app.use("/api/users", userRoute);
app.use("/api/groups", groupRoute);
app.use("/api/items", itemRoute);

app.use("/assets", express.static("client/dist/assets"));
app.use("*", express.static("client/dist"));
module.exports = app;
