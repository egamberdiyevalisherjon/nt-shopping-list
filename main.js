require("dotenv").config();

const mongoose = require("mongoose");

mongoose
  .connect(process.env.MONGODB_URI)
  .then(() => {
    console.log("MongoDb Connected");
  })
  .catch((err) => console.log("MondoDb Connection Error", err));

const app = require("./app");

const PORT = process.env.PORT || 5005;

const server = app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});

process.on("unhandledRejection", (err) => {
  console.log(err.name, err.message);
  console.log("UNHANDLED ERROR ");
  server.close(() => {
    process.exit(1);
  });
});
