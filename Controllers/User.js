const Group = require("../Models/Group");
const Item = require("../Models/Item");
const User = require("../Models/User");
const { catchAsync } = require("../Utils/catchAsync");
const { sign } = require("../Utils/jwt");
const { hashPassword, comparePassword } = require("../Utils/password");

exports.registerUser = catchAsync(async (req, res) => {
  const { name, username, password } = req.body;

  const existingUser = await User.findOne({ username, status: "active" });
  if (existingUser)
    return res.status(400).json({
      message: "User already exists",
    });

  const hashedPassword = await hashPassword(password);
  const newUserData = {
    name: name.toLowerCase(),
    username,
    password: hashedPassword,
  };
  const user = await User.create(newUserData);
  user.password = undefined;

  const token = sign({ _id: user._id });

  return res.status(201).json({
    message: "User is created successfully",
    token,
    user,
  });
});

exports.updateUser = catchAsync(async (req, res) => {
  const { _id } = req.user;
  const { name } = req.body;

  const user = await User.findByIdAndUpdate(
    _id,
    { name },
    { new: true }
  ).select("-password");

  return res.status(200).json({
    message: "User is updated successfully",
    user,
  });
});

exports.updateUsername = catchAsync(async (req, res) => {
  const { _id } = req.user;
  const { username } = req.body;

  const existingUser = await User.findOne({ username, status: "active" });
  if (existingUser)
    return res.status(400).json({
      message: "Username already in use",
    });

  const user = await User.findByIdAndUpdate(
    _id,
    { username },
    { new: true }
  ).select("-password");

  return res.status(200).json({
    message: "Username is updated successfully",
    user,
  });
});

exports.updatePassword = catchAsync(async (req, res) => {
  const { _id } = req.user;
  const { password, oldPassword } = req.body;

  const user = await User.findById(_id);

  if (!user) return res.status(404).json({ message: "User not found" });

  const isMatched = await comparePassword(oldPassword, user.password);

  if (!isMatched)
    return res.status(400).json({
      message: "Invalid password",
    });

  const hashedPassword = await hashPassword(password);
  user.password = hashedPassword;

  await user.save();

  return res.status(200).json({
    message: "Password is updated successfully",
  });
});

exports.deleteUser = catchAsync(async (req, res) => {
  const { _id } = req.user;

  const [ownedGroups, memberedGroups] = await Promise.all([
    await Group.find({ owner: _id }),
    await Group.find({ members: _id }),
  ]);

  const groupIdsToDelete = ownedGroups.map((group) => group._id);
  const groupIdsToLeave = memberedGroups.map((group) => group._id);
  const itemIds = ownedGroups.reduce(
    (ids, group) => [...ids, ...group.items.map((item) => item._id)],
    []
  );

  const queries = [
    User.findByIdAndUpdate(_id, { status: "deleted" }),
    Group.deleteMany({ _id: { $in: groupIdsToDelete } }),
    Item.deleteMany({ _id: { $in: itemIds } }),
  ];

  if (groupIdsToLeave.length) {
    queries.push(
      Group.updateMany(
        { _id: { $in: groupIdsToLeave } },
        { $pop: { members: _id } }
      )
    );
  }

  await Promise.all(queries);

  return res.status(200).json({
    message: "User is deleted successfully",
  });
});

exports.searchUser = catchAsync(async (req, res) => {
  const { q } = req.query;

  const users = await User.find({
    username: { $regex: q, $options: "i" },
    status: "active",
  }).select("-password");

  return res.status(200).json(users);
});
