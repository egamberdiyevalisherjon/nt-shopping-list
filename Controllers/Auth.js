const User = require("../Models/User");
const { catchAsync } = require("../Utils/catchAsync");
const { sign } = require("../Utils/jwt");
const { comparePassword } = require("../Utils/password");

exports.loginUser = catchAsync(async (req, res) => {
  const { username, password } = req.body;
  const user = await User.findOne({ username, status: "active" }).select();
  if (!user) return res.status(404).json({ message: "User not found" });

  const isMatched = await comparePassword(password, user.password);
  if (!isMatched)
    return res.status(400).json({ message: "Invalid credentials" });

  const token = sign({ _id: user._id });

  user.password = undefined;

  return res.status(200).json({
    message: "Logged in successfully",
    token,
    user,
  });
});

exports.getMe = catchAsync(async (req, res) => {
  const user = req.user;

  user.password = undefined;

  return res.status(200).json(user);
});
