const Group = require("../Models/Group");
const Item = require("../Models/Item");
const { catchAsync } = require("../Utils/catchAsync");
const { isSameObjectIds } = require("../Utils/Types");

exports.createItem = catchAsync(async (req, res) => {
  const { title, groupId } = req.body;
  const user = req.user;

  const group = await Group.findById(groupId).select("-password");
  if (!group) return res.status(404).json({ message: "Group not found" });

  const inGroup = group.members.find((m) => isSameObjectIds(m, user._id));
  if (!inGroup) return res.status(403).json({ message: "Not a group member" });

  const newItemData = { title, owner: user._id, group: groupId };

  const item = await Item.create(newItemData);

  group.items.push(item);

  await group.save();

  item.owner = user;

  return res
    .status(201)
    .json({ message: "Item is created successfully", item });
});

exports.updateItem = catchAsync(async (req, res) => {
  const { title } = req.body;
  const { itemId } = req.params;
  const user = req.user;

  const item = await Item.findById(itemId);

  if (!isSameObjectIds(item.owner, user._id))
    return res.status(403).json({
      message: "Not the owner of the item",
    });

  item.title = title;

  await item.save();

  return res
    .status(200)
    .json({ message: "Item is updated successfully", item });
});

exports.deleteItem = catchAsync(async (req, res) => {
  const { itemId } = req.params;
  const user = req.user;

  const item = await Item.findById(itemId);
  if (!item) return res.status(404).json({ message: "Item not found" });

  const group = await Group.findById(item.group);
  if (!group) return res.status(404).json({ message: "Group not found" });

  if (
    !isSameObjectIds(item.owner, user._id) &&
    !isSameObjectIds(group.owner, user._id)
  )
    return res.status(403).json({
      message: "Not the owner of the item or of the group",
    });

  group.items = group.items.filter((i) => !isSameObjectIds(i, item._id));

  const queries = [Item.findByIdAndDelete(item._id), group.save()];

  await Promise.all(queries);

  return res.status(200).json({ message: "Item is deleted successfully" });
});

exports.markItemAsBought = catchAsync(async (req, res) => {
  const { itemId } = req.params;
  const user = req.user;

  const item = await Item.findById(itemId);
  if (!item) return res.status(404).json({ message: "Item not found" });
  if (item.isBought) return res.status(400).json({ message: "Already bought" });

  const group = await Group.findById(item.group);
  if (!group) return res.status(404).json({ message: "Group not found" });

  const inGroup = group.members.find((m) => isSameObjectIds(m, user._id));
  if (!inGroup) return res.status(403).json({ message: "Not a group member" });

  item.isBought = true;
  item.boughtBy = user._id;
  item.boughtAt = Date.now();

  await item.save();

  return res.status(200).json({ message: "Marked as bought successfully" });
});

exports.markItemAsNotBought = catchAsync(async (req, res) => {
  const { itemId } = req.params;
  const user = req.user;

  const item = await Item.findById(itemId);
  if (!item) return res.status(404).json({ message: "Item not found" });
  if (!item.isBought)
    return res.status(400).json({ message: "Not bought yet" });

  const group = await Group.findById(item.group);
  if (!group) return res.status(404).json({ message: "Group not found" });

  if (
    !isSameObjectIds(item.boughtBy, user._id) &&
    !isSameObjectIds(group.owner, user._id)
  )
    return res.status(403).json({
      message: "Neither the buyer of the item nor the owner of the group",
    });

  item.isBought = false;
  item.boughtBy = undefined;
  item.boughtAt = undefined;

  await item.save();

  return res.status(200).json({ message: "Marked as not bought successfully" });
});
