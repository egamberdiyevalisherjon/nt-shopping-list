const { Types } = require("mongoose");
const Group = require("../Models/Group");
const Item = require("../Models/Item");
const User = require("../Models/User");
const { catchAsync } = require("../Utils/catchAsync");
const { hashPassword, comparePassword } = require("../Utils/password");
const { isSameObjectIds } = require("../Utils/Types");

exports.createGroup = catchAsync(async (req, res) => {
  const { name, password } = req.body;
  const user = req.user;

  const existingGroup = await Group.findOne({ name });
  if (existingGroup)
    return res.status(400).json({
      message: "Group name is not available",
    });

  const hashedPassword = await hashPassword(password);
  const newGroupData = {
    name,
    password: hashedPassword,
    owner: user._id,
    members: [user._id],
  };
  const group = await Group.create(newGroupData);
  group.password = undefined;
  group.owner = req.user;
  group.members = [req.user];

  return res.status(201).json({
    message: "Group is created successfully",
    group,
  });
});

exports.getMyGroups = catchAsync(async (req, res) => {
  const user = req.user;

  const groups = await Group.find({
    members: new Types.ObjectId(user._id),
  })
    .populate([
      { path: "members", select: "-password" },
      { path: "owner", select: "-password" },
      {
        path: "items",
        populate: [
          {
            path: "owner",
            select: "-password",
          },
          {
            path: "boughtBy",
            select: "-password",
          },
        ],
      },
    ])
    .select("-password");

  return res.status(200).json(groups);
});

exports.searchGroups = catchAsync(async (req, res) => {
  const { q } = req.query;

  const groups = await Group.find({
    name: { $regex: q, $options: "i" },
  })
    .select("name createdAt owner members")
    .populate({ path: "owner", select: "name" });

  return res.status(200).json(groups);
});

exports.addMemberToGroup = catchAsync(async (req, res) => {
  const { groupId } = req.params;
  const { memberId } = req.body;
  const user = req.user;

  const group = await Group.findById(groupId).select("-password");

  if (!group) return res.status(404).json({ message: "Group not found" });

  if (!isSameObjectIds(group.owner, user._id))
    return res
      .status(403)
      .json({ message: "You are not the owner of the group" });

  const member = await User.findById(memberId);
  if (!member) return res.status(404).json({ message: "User not found" });

  const memberIndex = group.members.findIndex((m) =>
    isSameObjectIds(m, memberId)
  );

  if (memberIndex !== -1)
    return res.status(400).json({ message: "Already in the group" });

  group.members.push(memberId);

  await group.save();

  return res.status(200).json({ message: "Added to group successfully" });
});

exports.removeMemberFromGroup = catchAsync(async (req, res) => {
  const { groupId, memberId } = req.params;
  const user = req.user;

  const group = await Group.findById(groupId).select("-password");

  if (!group) return res.status(404).json({ message: "Group not found" });
  if (!isSameObjectIds(group.owner, user._id))
    return res
      .status(403)
      .json({ message: "You are not the owner of the group" });

  const member = await User.findById(memberId);
  if (!member) return res.status(404).json({ message: "User not found" });

  const memberIndex = group.members.findIndex((m) =>
    isSameObjectIds(m, memberId)
  );

  if (memberIndex === -1)
    return res.status(400).json({ message: "Not a group member" });

  group.members.splice(memberIndex, 1);

  await group.save();

  return res.status(200).json({ message: "Removed from group successfully" });
});

exports.joinToGroup = catchAsync(async (req, res) => {
  const { groupId } = req.params;
  const { password } = req.body;
  const user = req.user;

  const group = await Group.findById(groupId).populate([
    { path: "members", select: "-password" },
    { path: "owner", select: "-password" },
    {
      path: "items",
      populate: [
        {
          path: "owner",
          select: "-password",
        },
        {
          path: "boughtBy",
          select: "-password",
        },
      ],
    },
  ]);
  if (!group) return res.status(404).json({ message: "Group not found" });

  const memberIndex = group.members.findIndex((m) =>
    isSameObjectIds(m, user._id)
  );
  if (memberIndex !== -1)
    return res.status(400).json({ message: "Already in the group" });

  const isMatched = await comparePassword(password, group.password);
  if (!isMatched) return res.status(400).json({ message: "Invalid password" });

  group.members.push(user);

  await group.save();

  group.password = undefined;
  return res
    .status(200)
    .json({ message: "Joined to group successfully", group });
});

exports.leaveFromGroup = catchAsync(async (req, res) => {
  const { groupId } = req.params;
  const user = req.user;

  const group = await Group.findById(groupId).select("-password");

  if (!group) return res.status(404).json({ message: "Group not found" });
  if (isSameObjectIds(group.owner, user._id))
    return res.status(403).json({ message: "Owners cannot leave the group" });

  const memberIndex = group.members.findIndex((m) =>
    isSameObjectIds(m, user._id)
  );

  if (memberIndex === -1)
    return res.status(400).json({ message: "Not a group member" });

  group.members.splice(memberIndex, 1);

  await group.save();

  return res.status(200).json({ message: "Left from group successfully" });
});

exports.deleteGroup = catchAsync(async (req, res) => {
  const { groupId } = req.params;
  const user = req.user;

  const group = await Group.findById(groupId).select("-password");

  if (!group) return res.status(404).json({ message: "Group not found" });

  if (!isSameObjectIds(group.owner, user._id))
    return res
      .status(403)
      .json({ message: "You are not the owner of the group" });

  const queries = [
    Group.findByIdAndDelete(groupId),
    Item.deleteMany({ _id: { $in: group.items.map((item) => item._id) } }),
  ];

  await Promise.all(queries);

  return res.status(200).json({ message: "Group is deleted successfully" });
});
