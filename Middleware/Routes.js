const User = require("../Models/User");
const { decode } = require("../Utils/jwt");
const { validationResult } = require("express-validator");

exports.validatedRoute = (req, res, next) => {
  const result = validationResult(req);
  if (!result.isEmpty()) {
    return res
      .status(400)
      .send({ message: "Invalid credentials", errors: result.array() });
  }

  next();
};

exports.privateRoute = async (req, res, next) => {
  const token = req.headers["x-auth-token"];

  if (!token)
    return res.status(401).json({
      message: "Unauthorized",
    });

  const decoded = decode(token);
  if (!decoded) return res.status(401).json({ message: "Unauthorized" });

  const { _id } = decoded;
  const user = await User.findById(_id).select("-password");

  if (!user) return res.status(404).json({ message: "User not found" });

  req.user = user;

  next();
};
