exports.normalizeQuery = (req, res, next) => {
  let query = req.query.q;
  let validQuery = ''

  const chars = {
    "\\": 1,
    "*": 1,
    "-": 1,
    "?": 1,
  };

  for (let letter of query) {
    if (!chars[letter]) validQuery += letter;
  }

  if (!validQuery) return res.status(200).json([]);

  req.query.q = validQuery;

  next();
};
