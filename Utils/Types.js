exports.isSameObjectIds = (id1, id2) => String(id1) === String(id2);
