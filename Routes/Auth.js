const express = require("express");
const { body } = require("express-validator");
const { loginUser, getMe } = require("../Controllers/Auth");
const { validatedRoute, privateRoute } = require("../Middleware/Routes");

const router = express.Router();

router.post(
  "/",
  body("username").isString().withMessage("Username is required"),
  body("password").isString().withMessage("Password is required"),
  validatedRoute,
  loginUser
);

router.get("/", privateRoute, getMe);

module.exports = authRoute = router;
