const express = require("express");
const { body, query } = require("express-validator");
const {
  registerUser,
  updateUser,
  updateUsername,
  updatePassword,
  deleteUser,
  searchUser,
} = require("../Controllers/User");
const { validatedRoute, privateRoute } = require("../Middleware/Routes");
const { normalizeQuery } = require("../Middleware/Validation");

const router = express.Router();

router
  .route("/")
  .post(
    body("name").trim().isString().withMessage("Name is required"),
    body("username")
      .isString()
      .withMessage("Username is required")
      .isLength({ min: 4 })
      .withMessage("Username must be at least 4 characters long"),
    body("password")
      .isString()
      .withMessage("Password is required")
      .isLength({ min: 6 })
      .withMessage("Password must be at least 6 characters long"),
    validatedRoute,
    registerUser
  )
  .put(
    privateRoute,
    body("name").isString().withMessage("Name is required"),
    validatedRoute,
    updateUser
  )
  .delete(privateRoute, deleteUser);

router.put(
  "/username",
  privateRoute,
  body("username")
    .isString()
    .withMessage("Username is required")
    .isLength({ min: 4 })
    .withMessage("Username must be at least 4 characters long"),
  validatedRoute,
  updateUsername
);

router.put(
  "/password",
  privateRoute,
  body("oldPassword").isString().withMessage("Old password is required"),
  body("password")
    .isString()
    .withMessage("Password is required")
    .isLength({ min: 6 })
    .withMessage("Password must be at least 6 characters long"),
  validatedRoute,
  updatePassword
);

router.get(
  "/search",
  query("q").isString().withMessage("Search text is required"),
  validatedRoute,
  normalizeQuery,
  searchUser
);

module.exports = userRoute = router;
