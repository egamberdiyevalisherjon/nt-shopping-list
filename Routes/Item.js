const express = require("express");
const { body, param } = require("express-validator");
const { Types } = require("mongoose");
const {
  createItem,
  updateItem,
  deleteItem,
  markItemAsBought,
  markItemAsNotBought,
} = require("../Controllers/Item");
const { validatedRoute, privateRoute } = require("../Middleware/Routes");

const router = express.Router();

router.post(
  "/",
  privateRoute,
  body("title").isString().withMessage("Title is required"),
  body("groupId")
    .exists()
    .withMessage("Group ID is required")
    .custom((value) => Types.ObjectId.isValid(value))
    .withMessage("Invalid ID"),
  validatedRoute,
  createItem
);

router
  .route("/:itemId")
  .put(
    privateRoute,
    body("title").isString().withMessage("Title is required"),
    param("itemId")
      .custom((value) => Types.ObjectId.isValid(value))
      .withMessage("Invalid ID"),
    validatedRoute,
    updateItem
  )
  .delete(
    privateRoute,
    param("itemId")
      .custom((value) => Types.ObjectId.isValid(value))
      .withMessage("Invalid ID"),
    validatedRoute,
    deleteItem
  );

router
  .route("/:itemId/mark-as-bought")
  .post(
    privateRoute,
    param("itemId")
      .custom((value) => Types.ObjectId.isValid(value))
      .withMessage("Invalid ID"),
    validatedRoute,
    markItemAsBought
  )
  .delete(
    privateRoute,
    param("itemId")
      .custom((value) => Types.ObjectId.isValid(value))
      .withMessage("Invalid ID"),
    validatedRoute,
    markItemAsNotBought
  );

module.exports = itemRoute = router;
