const express = require("express");
const { body, query, param } = require("express-validator");
const { Types } = require("mongoose");
const {
  createGroup,
  searchGroups,
  getMyGroups,
  addMemberToGroup,
  removeMemberFromGroup,
  deleteGroup,
  joinToGroup,
  leaveFromGroup,
} = require("../Controllers/Group");
const { validatedRoute, privateRoute } = require("../Middleware/Routes");
const { normalizeQuery } = require("../Middleware/Validation");

const router = express.Router();

router
  .route("/")
  .get(privateRoute, getMyGroups)
  .post(
    body("name").isString().withMessage("Name is required"),
    body("password")
      .isString()
      .withMessage("Password is required")
      .isLength({ min: 6 })
      .withMessage("Password must be at least 6 characters long"),
    validatedRoute,
    privateRoute,
    createGroup
  );

router.route("/:groupId").delete(privateRoute, deleteGroup);

router.route("/:groupId/members").post(
  privateRoute,
  body("memberId")
    .exists()
    .withMessage("Member ID is required")
    .custom((value) => Types.ObjectId.isValid(value))
    .withMessage("Invalid ID"),
  validatedRoute,
  addMemberToGroup
);

router.route("/:groupId/members/:memberId").delete(
  privateRoute,
  param("memberId")
    .exists()
    .withMessage("Member ID is required")
    .custom((value) => Types.ObjectId.isValid(value))
    .withMessage("Invalid ID"),
  validatedRoute,
  removeMemberFromGroup
);

router
  .route("/:groupId/join")
  .post(
    privateRoute,
    body("password").isString().withMessage("Password is required"),
    validatedRoute,
    joinToGroup
  );
router.route("/:groupId/leave").post(privateRoute, leaveFromGroup);

router.get(
  "/search",
  privateRoute,
  query("q").isString().withMessage("Search text is required"),
  validatedRoute,
  normalizeQuery,
  searchGroups
);

module.exports = groupRoute = router;
