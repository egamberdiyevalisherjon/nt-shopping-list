import { toast } from "react-toastify";

export const handleHTTPError = (error) => {
  if (error.response?.data?.message)
    toast(error.response?.data?.message, { type: "error" });
  if (error.response?.data?.errors)
    error.response?.data?.errors.forEach((err) =>
      toast(err.msg, { type: "error" })
    );
};
