import { format } from 'date-fns'

export const formatDate = (date) => format(date, "hh:mm,dd-MM-yyyy");