import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  groups: [],
};

const groupSlice = createSlice({
  name: "group-slice",
  initialState,
  reducers: {
    setGroups(state, { payload }) {
      state.groups = payload;
    },
    addGroup(state, { payload }) {
      state.groups.push(payload);
    },
    removeGroup(state, { payload }) {
      state.groups = state.groups.filter((group) => group._id !== payload);
    },
    addMemberToGroup(state, { payload: { groupId, member } }) {
      state.groups.find((group) => group._id === groupId)?.members.push(member);
    },
    removeMemberFromGroup(state, { payload: { groupId, memberId } }) {
      const group = state.groups.find((group) => group._id === groupId);
      if (group)
        group.members = group.members.filter(
          (member) => member._id !== memberId
        );
    },
    removeItemFromGroup(state, { payload: { groupId, itemId } }) {
      const group = state.groups.find((group) => group._id === groupId);
      if (group)
        group.items = group.items.filter((item) => item._id !== itemId);
    },
    addItem(state, { payload: { groupId, item } }) {
      state.groups.find((group) => group._id === groupId)?.items.push(item);
    },
    resetGroups(state) {
      state.groups = [];
    },
  },
});

export default groupSlice.reducer;

export const {
  setGroups,
  addGroup,
  removeMemberFromGroup,
  removeItemFromGroup,
  addItem,
  addMemberToGroup,
  resetGroups,
  removeGroup,
} = groupSlice.actions;
