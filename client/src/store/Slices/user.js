import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  info: null,
};

const userSlice = createSlice({
  name: "user-slice",
  initialState,

  reducers: {
    setUser(state, action) {
      state.info = action.payload;
    },
    resetUser(state) {
      state.info = null;
    },
  },
});

export default userSlice.reducer;

export const { setUser, resetUser } = userSlice.actions;
