import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./Slices/user";
import groupsReducer from "./Slices/group";

const store = configureStore({
  reducer: {
    user: userReducer,
    group: groupsReducer,
  },
});

export default store;
