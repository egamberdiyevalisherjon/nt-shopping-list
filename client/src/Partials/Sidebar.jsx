import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useMutation, useQuery } from "react-query";
import axios from "axios";
import { handleHTTPError } from "../Utils/api";
import { addGroup, setGroups } from "../store/Slices/group";
import { OverlayTrigger, Popover, CloseButton } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";

const Sidebar = () => {
  const [groupsCollapsed, setGroupsCollapsed] = useState(false);
  const [showAddGroupPopover, setShowAddGroupPopover] = useState(false);
  const groups = useSelector((store) => store.group.groups);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useQuery(["my", "groups"], () => axios.get("/groups"), {
    onError: handleHTTPError,
    onSuccess: ({ data }) => {
      dispatch(setGroups(data));
    },
  });

  const createGroupMutation = useMutation({
    mutationFn: (newGroup) => axios.post("/groups", newGroup),
    onError: handleHTTPError,
    onSuccess: ({ data: { message, group } }) => {
      toast(message, { type: "success" });
      dispatch(addGroup(group));
      navigate(`/main/groups/${group._id}`);
      setShowAddGroupPopover(false);
    },
  });

  const { register, handleSubmit } = useForm();

  const onCreateGroup = (values) => {
    createGroupMutation.mutate(values);
  };

  return (
    <aside id="main-sidebar" className="border-end py-3 bg-white overflow-auto">
      <nav>
        <ul className="list-unstyled px-2 d-grid gap-3">
          <li>
            <Link
              className="navigation-link btn btn-light w-100 text-start"
              to={"/main"}
            >
              <i className="fa-solid fa-user text-primary" /> Profile
            </Link>
          </li>
          <li className="d-grid gap-3">
            <button
              className="navigation-link btn btn-light w-100 text-start"
              onClick={() => setGroupsCollapsed(!groupsCollapsed)}
            >
              <i className="fa-regular fa-comments text-primary" /> Groups{" "}
              <i
                className={`fa-solid fa-chevron-${
                  groupsCollapsed ? "left" : "down"
                }`}
              ></i>
            </button>
            <ul
              id="group-links-wrapper"
              className={`list-unstyled ps-3 d-grid gap-3 ${
                groupsCollapsed ? "collapsed" : ""
              }`}
            >
              <li>
                <OverlayTrigger
                  trigger="click"
                  placement="right"
                  show={showAddGroupPopover}
                  onToggle={() => setShowAddGroupPopover(!showAddGroupPopover)}
                  overlay={
                    <Popover id="popover-basic">
                      <Popover.Header as="h2">
                        Group name and password
                        <CloseButton
                          onClick={() => setShowAddGroupPopover(false)}
                        />
                      </Popover.Header>
                      <Popover.Body>
                        <form
                          className="d-grid gap-2"
                          onSubmit={handleSubmit(onCreateGroup)}
                        >
                          <input
                            {...register("name")}
                            className="form-control"
                            type="text"
                            placeholder="Group name"
                          />
                          <input
                            {...register("password")}
                            className="form-control"
                            type="password"
                            placeholder="Group password"
                          />
                          <div className="d-flex gap-3">
                            <button className="btn btn-primary flex-fill">
                              Create
                            </button>
                            <button
                              type="button"
                              onClick={() => setShowAddGroupPopover(false)}
                              className="btn btn-outline-primary flex-fill"
                            >
                              Cancel
                            </button>
                          </div>
                        </form>
                      </Popover.Body>
                    </Popover>
                  }
                >
                  <button className="navigation-link btn btn-light w-100 text-start">
                    <i className="fa-solid fa-add text-primary"></i> Create
                    Group
                  </button>
                </OverlayTrigger>
              </li>
              {groups.map((group) => {
                return (
                  <li key={group._id}>
                    <Link
                      className="navigation-link btn btn-light w-100 text-start"
                      to={`/main/groups/${group._id}`}
                    >
                      {group.name}
                    </Link>
                  </li>
                );
              })}
            </ul>
          </li>
        </ul>
      </nav>
    </aside>
  );
};

export default Sidebar;
