import axios from "axios";
import { useState } from "react";
import {
  Dropdown,
  OverlayTrigger,
  Popover,
  CloseButton,
} from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { localTokenKey, reqTokenHederKey } from "../constants";
import { addGroup, resetGroups } from "../store/Slices/group";
import { resetUser } from "../store/Slices/user";
import { useMutation, useQuery } from "react-query";
import { handleHTTPError } from "../Utils/api";
import { formatDate } from "../Utils/date";

const SettingsDropDown = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleLogout = () => {
    localStorage.removeItem(localTokenKey);
    dispatch(resetUser());
    dispatch(resetGroups());
    localStorage.removeItem(localTokenKey);
    axios.defaults.headers.common[reqTokenHederKey] = undefined;
    toast("Logged out successfully", { type: "success" });
    navigate("/login");
  };

  return (
    <>
      <Dropdown>
        <Dropdown.Toggle split={false} variant="white" id="dropdown-basic">
          <i className="fa-solid fa-cog"></i>
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item onClick={handleLogout}>Log out</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
};

const GroupListItem = ({ group, setSearchedGroup, setGroupList }) => {
  const [showPasswordModal, setShowPasswordModal] = useState(false);
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const joinGroupMutation = useMutation({
    mutationFn: (groupId) =>
      axios.post(`/groups/${groupId}/join`, { password }),
    onError: handleHTTPError,
    onSuccess: ({ data: { message, group } }) => {
      setSearchedGroup("");
      setGroupList([]);
      setShowPasswordModal(false);
      toast(message, { type: "success" });
      dispatch(addGroup(group));
      navigate(`/main/groups/${group._id}`);
    },
  });

  const handleJoinGroup = (id) => {
    if (!password) return toast("Password is required!", { type: "error" });

    joinGroupMutation.mutate(id);
  };

  return (
    <li className="list-group-item d-flex justify-content-between align-items-center">
      <div>
        <div className="d-flex align-items-start gap-1">
          <h5>{group.name}</h5>{" "}
          <span className="badge bg-info">{formatDate(group.createdAt)}</span>
        </div>
        <p className="text-capitalize">Created By {group.owner.name}</p>
      </div>
      <OverlayTrigger
        trigger="click"
        placement="right"
        show={showPasswordModal}
        onToggle={() => {
          setPassword("");
          setShowPasswordModal(!showPasswordModal);
        }}
        overlay={
          <Popover id={`join-to-group-${group._id}`}>
            <Popover.Header
              className="d-flex justify-content-between align-items-center"
              as="h2"
            >
              Group password
              <CloseButton onClick={() => setShowPasswordModal(false)} />
            </Popover.Header>
            <Popover.Body>
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleJoinGroup(group._id);
                }}
                className="d-grid gap-2"
              >
                <input
                  type="password"
                  className="form-control"
                  placeholder="******"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <button className="btn btn-success">Join Group</button>
              </form>
            </Popover.Body>
          </Popover>
        }
      >
        <button className="btn btn-primary">Join</button>
      </OverlayTrigger>
    </li>
  );
};

const SearchGroupField = () => {
  const [groupList, setGroupList] = useState([]);
  const [searchedGroup, setSearchedGroup] = useState("");

  const user = useSelector((store) => store.user.info);

  useQuery(
    ["search", "groups", searchedGroup],
    () => axios.get(`/groups/search?q=${searchedGroup}`),
    {
      enabled: !!searchedGroup,
      onError: handleHTTPError,
      onSuccess: ({ data: groups }) => {
        setGroupList(
          groups.filter(
            (group) => !group.members.find((member) => member === user._id)
          )
        );
      },
    }
  );

  const handleSearchGroup = (e) => {
    const groupName = e.target.value.trim();
    setSearchedGroup(groupName);
    if (!groupName) setGroupList([]);
  };

  return (
    <div className="d-flex w-50 position-relative">
      <input
        id="search-group"
        type="search"
        className="form-control"
        placeholder="Search group and join..."
        onChange={handleSearchGroup}
        value={searchedGroup}
      />
      {searchedGroup && (
        <div className="position-absolute top-100 w-100 mt-1 bg-light p-3 rounded border shadow-lg">
          <h3>{groupList.length ? "Groups" : "No group found😢"}</h3>
          <ul className="list-group">
            {groupList.map((group) => (
              <GroupListItem
                key={group._id}
                group={group}
                setGroupList={setGroupList}
                setSearchedGroup={setSearchedGroup}
              />
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

const Header = () => {
  return (
    <header
      id="main-header"
      className="py-2 shadow d-flex align-items-center bg-white sticky-top"
    >
      <nav className="container-fluid d-flex justify-content-between">
        <div className="d-flex gap-3 align-items-center">
          <Link className="btn" to="/main">
            <i className="fa-solid fa-blog fa-2x text-primary"></i>
          </Link>
          <button className="btn btn-primary rounded-pill py-1 px-4 shadow d-none d-md-block">
            + New
          </button>
        </div>
        <SearchGroupField />
        <ul className="list-unstyled d-flex gap-3 m-0">
          <li className="d-flex"></li>
          <li>
            <button className="btn rounded-circle">
              <i className="fa-solid fa-rotate"></i>
            </button>
          </li>
          <li>
            <button className="btn rounded-circle position-relative">
              <i className="fa-regular fa-bell"></i>
              <span className="badge bg-danger position-absolute rounded-pill">
                9+
              </span>
            </button>
          </li>
          <li>
            <SettingsDropDown />
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
