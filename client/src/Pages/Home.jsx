import axios from "axios";
import { useMutation } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { localTokenKey, reqTokenHederKey } from "../constants";
import { resetGroups } from "../store/Slices/group";
import { resetUser } from "../store/Slices/user";
import { handleHTTPError } from "../Utils/api";

const Home = () => {
  const user = useSelector((store) => store.user.info);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const deleteAccountMutation = useMutation({
    mutationFn: () => axios.delete(`/users`),
    onError: handleHTTPError,
    onSuccess: ({ data: { message } }) => {
      toast(message, { type: "success" });
      dispatch(resetUser());
      dispatch(resetGroups());
      localStorage.removeItem(localTokenKey);
      axios.defaults.headers.common[reqTokenHederKey] = undefined;
      navigate("/login");
    },
  });

  if (!user) return;

  const handleDeleteAccount = () =>
    confirm(
      "Are you sure to delete your account?\nYou cannot undo this action!"
    ) && deleteAccountMutation.mutate();

  const handleCopyUsername = () => {
    if (navigator.clipboard) {
      navigator.clipboard.writeText(user.username);
      toast("Username is copied.", { type: "success" });
    }
  };

  return (
    <section className="bg-white p-4 py-3 rounded-5">
      <div className="d-flex justify-content-between align-items-center">
        <h1>Your Profile</h1>
        <div className="d-flex gap-1">
          <button className="btn btn-primary" onClick={handleCopyUsername}>
            <i className="fa-solid fa-copy"></i> Copy Username
          </button>
          <button className="btn btn-danger" onClick={handleDeleteAccount}>
            <i className="fa-solid fa-trash"></i> Delete Account
          </button>
        </div>
      </div>
      <form>
        <div className="row align-items-center">
          <div className="col-lg-2">
            <div
              id="profile-avatar"
              className="display-1 border border-3 text-bg-danger rounded-circle w-100"
            >
              {user.name[0].toUpperCase()}
            </div>
          </div>
          <div className="col-lg-10">
            <h2 className="text-capitalize d-flex align-items-start gap-3">
              {user.name}{" "}
              <span
                className={`fs-6 badge bg-${
                  user.status === "active" ? "success" : "danger"
                }`}
              >
                {user.status}
              </span>
            </h2>
            <p className="text-secondary">{user.username}</p>
          </div>
        </div>
      </form>
    </section>
  );
};

export default Home;
