import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const NotFound = () => {
  const user = useSelector(store => store.user.info)

  const backUrl = user ? "/main" : "/login"

  return (
    <section className="text-center min-vh-100 pt-5">
      <img src="/assets/not-found.svg" alt="" />
      <h1 className="display-1 fw-bold">Page Not Found</h1>
      <Link to={backUrl}>Back to home</Link>
    </section>
  );
};

export default NotFound;
