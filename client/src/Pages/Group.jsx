import axios from "axios";
import { useForm } from "react-hook-form";
import { useMutation } from "react-query";
import { useDispatch, useSelector } from "react-redux";
import { Navigate, useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import {
  addItem,
  addMemberToGroup,
  removeGroup,
  removeItemFromGroup,
  removeMemberFromGroup,
  setGroups,
} from "../store/Slices/group";
import { handleHTTPError } from "../Utils/api";
import { formatDate } from "../Utils/date";
import { Button, Dropdown, Modal } from "react-bootstrap";
import { useEffect, useState } from "react";

const AddItemForm = () => {
  const { register, handleSubmit, reset } = useForm();
  const { groupId } = useParams();
  const dispatch = useDispatch();

  const createItemMutation = useMutation({
    mutationFn: (title) => axios.post("/items", { title, groupId }),
    onError: handleHTTPError,
    onSuccess: ({ data: { message, item } }) => {
      toast(message, { type: "success" });
      dispatch(addItem({ groupId, item }));
      reset();
    },
  });

  const onCreateItem = (values) => {
    createItemMutation.mutate(values.title);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onCreateItem)}>
        <div className="d-flex gap-1">
          <input
            type="text"
            placeholder="Title"
            className="form-control"
            {...register("title")}
          />
          <button className="btn btn-primary">
            <i className="fa-solid fa-plus"></i>
          </button>
        </div>
      </form>
    </>
  );
};

const AddMemberModal = ({ show, onHide }) => {
  const [searchText, setSearchText] = useState("");
  const [users, setUsers] = useState([]);

  const dispatch = useDispatch();

  useEffect(() => {
    if (searchText) {
      axios
        .get(`/users/search?q=${searchText}`)
        .then(({ data }) => setUsers(data));
    } else {
      setUsers(() => []);
    }
  }, [searchText]);

  function handleHide() {
    setSearchText("");
    setUsers([]);
    onHide();
  }

  const { groupId } = useParams();

  const group = useSelector((store) =>
    store.group.groups.find((group) => group._id === groupId)
  );

  const addMemberMutation = useMutation({
    mutationFn: (member) =>
      axios.post(`/groups/${groupId}/members`, { memberId: member._id }),
    onError: handleHTTPError,
    onSuccess: ({ data: { message } }, member) => {
      toast(message, { type: "success" });
      dispatch(addMemberToGroup({ groupId, member }));
      handleHide();
    },
  });

  function handleAddMember(member) {
    addMemberMutation.mutate(member);
  }

  return (
    <Modal
      show={show}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      backdrop="static"
      onHide={handleHide}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Add Member</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <input
          type="search"
          className="form-control"
          value={searchText}
          onChange={(e) => setSearchText(e.target.value)}
          placeholder="Search user"
        />
        <ul className="list-group my-3" style={{ height: "300px" }}>
          {users
            .filter(
              (user) => !group.members.find((member) => member._id === user._id)
            )
            .map((user) => (
              <li
                key={user._id}
                onClick={() => handleAddMember(user)}
                className="list-group-item text-capitalize cursor-pointer"
                role="button"
              >
                {user.name}
              </li>
            ))}
        </ul>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={handleHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
};

const GroupDropDown = ({ group }) => {
  const [showAddMemberModal, setShowAddMemberModal] = useState(false);

  const { groupId } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const user = useSelector((store) => store.user.info);

  const deleteGroupMutation = useMutation({
    mutationFn: () => axios.delete(`/groups/${groupId}`),
    onError: handleHTTPError,
    onSuccess: ({ data: { message } }) => {
      toast(message, { type: "success" });
      dispatch(removeGroup(groupId));
      navigate("/main");
    },
  });

  const leaveGroupMutation = useMutation({
    mutationFn: () => axios.post(`/groups/${groupId}/leave`),
    onError: handleHTTPError,
    onSuccess: ({ data: { message } }) => {
      toast(message, { type: "success" });
      dispatch(removeGroup(groupId));
      navigate("/main");
    },
  });

  const handleDeleteGroup = () =>
    confirm(
      "Are you sure to delete this group?\nYou cannot undo this action!"
    ) && deleteGroupMutation.mutate();

  const handleLeaveGroup = () => {
    leaveGroupMutation.mutate();
  };

  return (
    <>
      <Dropdown>
        <Dropdown.Toggle split={false} variant="light" id="dropdown-basic">
          <i className="fa-solid fa-ellipsis"></i>
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item onClick={() => setShowAddMemberModal(true)}>
            Add member
          </Dropdown.Item>
          {group.owner._id === user._id ? (
            <Dropdown.Item onClick={handleDeleteGroup}>
              Delete Group
            </Dropdown.Item>
          ) : (
            <Dropdown.Item onClick={handleLeaveGroup}>
              Leave Group
            </Dropdown.Item>
          )}
        </Dropdown.Menu>
      </Dropdown>
      <AddMemberModal
        show={showAddMemberModal}
        onHide={() => setShowAddMemberModal(false)}
      />
    </>
  );
};

const Group = () => {
  const { groupId } = useParams();

  const dispatch = useDispatch();

  const groups = useSelector((store) => store.group.groups);
  const group = groups.find((group) => group._id === groupId);
  const user = useSelector((store) => store.user.info);

  const removeMemberFromGroupMutation = useMutation({
    onError: handleHTTPError,
    onSuccess: ({ data: { message } }, memberId) => {
      toast(message, { type: "success" });
      dispatch(removeMemberFromGroup({ memberId, groupId }));
    },
    mutationFn: (memberId) =>
      axios.delete(`/groups/${groupId}/members/${memberId}`),
  });

  const buyItemMutation = useMutation({
    onError: handleHTTPError,
    onSuccess: async ({ data: { message } }) => {
      const { data: groups } = await axios.get(`/groups`);
      dispatch(setGroups(groups));
      toast(message, { type: "success" });
    },
    mutationFn: (itemId) => axios.post(`/items/${itemId}/mark-as-bought`),
  });

  const unBuyItemMutation = useMutation({
    onError: handleHTTPError,
    onSuccess: async ({ data: { message } }) => {
      const { data: groups } = await axios.get(`/groups`);
      dispatch(setGroups(groups));
      toast(message, { type: "success" });
    },
    mutationFn: (itemId) => axios.delete(`/items/${itemId}/mark-as-bought`),
  });

  const deleteItemMutation = useMutation({
    onError: handleHTTPError,
    onSuccess: async ({ data: { message } }, itemId) => {
      dispatch(removeItemFromGroup({ groupId, itemId }));
      toast(message, { type: "success" });
    },
    mutationFn: (itemId) => axios.delete(`/items/${itemId}`),
  });

  const handleDeleteItem = (itemId) => {
    deleteItemMutation.mutate(itemId);
  };

  const handleBuyItem = (itemId) => {
    buyItemMutation.mutate(itemId);
  };

  const handleUnBuyItem = (itemId) => {
    unBuyItemMutation.mutate(itemId);
  };

  const handleRemoveMember = (memberId) => {
    removeMemberFromGroupMutation.mutate(memberId);
  };

  if (!group) {
    if (groups.length) return <Navigate to="/main" />;
    return;
  }

  return (
    <div className="h-100 d-flex flex-column overflow-hidden">
      <div className="d-flex justify-content-between align-items-center gap-3">
        <h1 className="text-white">{group.name}</h1>
        <div className="d-flex justify-content-between align-items-center gap-3">
          <div className="bg-light p-2 rounded d-lg-flex gap-3 d-none">
            <span>Owner: </span>{" "}
            <span className="text-capitalize d-flex align-items-center gap-1">
              <span className="badge bg-primary">{group.owner.name[0]}</span>{" "}
              <span>
                <span>{group.owner.name}</span>(
                <span className="text-secondary">{group.owner.username}</span>)
              </span>
            </span>
          </div>
          <GroupDropDown group={group} />
        </div>
      </div>
      <div className="row flex-fill overflow-hidden gx-5">
        <div className="col-xl-6 h-100 overflow-hidden d-flex flex-column">
          <div className="p-3 bg-white rounded-top-3 d-flex flex-column gap-1 h-100">
            <div className="d-flex justify-content-between">
              <h4>
                Items{" "}
                <span className="badge bg-primary">{group.items.length}</span>
              </h4>
              <AddItemForm />
            </div>
            <div className="h-100 overflow-auto">
              <ul className="list-group">
                {group.items.map((item) => (
                  <li
                    className="list-group-item text-capitalize d-flex justify-content-between align-items-start gap-3 flex-column flex-md-row"
                    key={item._id}
                  >
                    <div className="d-flex align-items-center gap-3">
                      <span className="badge bg-primary fs-5">
                        {item.title[0]}
                      </span>{" "}
                      <div>
                        <span>{item.title} </span>{" "}
                        {item.isBought && (
                          <span className="badge bg-info">
                            Bought by {item.boughtBy.name}{" "}
                            {formatDate(item.boughtAt)}
                          </span>
                        )}
                        <br />
                        <span className="text-secondary">
                          Created by {item.owner.name} (
                          {formatDate(item.createdAt)})
                        </span>
                      </div>
                    </div>
                    <div className="d-flex gap-1">
                      {!item.isBought ? (
                        <button
                          className="btn btn-success"
                          onClick={() => handleBuyItem(item._id)}
                        >
                          <i className="fa-solid fa-cart-plus"></i>
                        </button>
                      ) : (
                        (item.boughtBy._id === user._id ||
                          group.owner._id === user._id) && (
                          <button
                            className="btn btn-warning"
                            onClick={() => handleUnBuyItem(item._id)}
                          >
                            <i className="fa-solid fa-shop-slash"></i>
                          </button>
                        )
                      )}
                      {(group.owner._id === user._id ||
                        item.owner._id === user._id) && (
                        <button
                          className="btn btn-danger"
                          onClick={() => handleDeleteItem(item._id)}
                        >
                          <i className="fa-solid fa-x"></i>
                        </button>
                      )}
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
        <div className="d-none col-xl-6 h-100 overflow-hidden d-xl-flex flex-column">
          <div className="p-3 bg-white rounded-top-3 d-flex flex-column gap-1 h-100">
            <h4>
              Members{" "}
              <span className="badge bg-primary">{group.members.length}</span>
            </h4>
            <div className="h-100 overflow-auto">
              <ul className="list-group">
                {group.members.map((member) => (
                  <li
                    className="list-group-item text-capitalize d-flex justify-content-between align-items-start gap-3"
                    key={member._id}
                  >
                    <div className="d-flex align-items-center gap-3">
                      <span className="badge bg-primary fs-5">
                        {member.name[0]}
                      </span>{" "}
                      <div>
                        <span>{member.name}</span>
                        <br />
                        <span className="text-secondary">
                          {member.username}
                        </span>
                      </div>
                    </div>
                    {group.owner._id === user._id &&
                      member._id !== user._id && (
                        <button
                          className="btn btn-danger"
                          onClick={() => handleRemoveMember(member._id)}
                        >
                          <i className="fa-solid fa-x"></i>
                        </button>
                      )}
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Group;
