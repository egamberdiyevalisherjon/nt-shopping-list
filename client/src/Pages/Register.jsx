import { Link, Navigate, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useMutation, useQuery } from "react-query";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { setUser } from "../store/Slices/user";
import { localTokenKey, reqTokenHederKey } from "../constants";

const Register = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { register, handleSubmit, getValues } = useForm();

  const { refetch: searchUsername, data } = useQuery(
    ["search", "user"],
    async () => axios.get(`/users/search?q=${getValues("username")}`),
    { enabled: false }
  );

  const mutation = useMutation({
    mutationFn: (user) => {
      return axios.post("/users", user);
    },
    onError: (error) => {
      if (error.response?.data?.message)
        toast(error.response?.data?.message, { type: "error" });
      else if (error.response?.data?.errors)
        error.response?.data?.errors.forEach((err) =>
          toast(err.msg, { type: "error" })
        );
    },
    onSuccess: ({ data: { message, user, token } }) => {
      toast(message, { type: "success" });
      dispatch(setUser(user));
      axios.defaults.headers.common[reqTokenHederKey] = token;
      localStorage.setItem(localTokenKey, token);
      navigate("/main");
    },
  });

  const onRegister = async (values) => {
    const {
      data: { data: users },
    } = await searchUsername();
    if (users.length)
      return toast("Username is unavailable", { type: "error" });
    await mutation.mutateAsync(values);
  };

  const token = localStorage.getItem(localTokenKey);

  if (token) return <Navigate to={"/main"} />;

  return (
    <section className="bg-secondary vh-100 d-flex align-items-center">
      <div className="container">
        <div className="row bg-white gx-3 rounded-4 overflow-hidden">
          <div className="col-md-6 text-center text-bg-dark p-5">
            <i className="fa-solid fa-blog fa-8x text-primary"></i>
            <p className="mt-5">Welcome to</p>
            <h1 className="display-1">Shopping List</h1>
          </div>
          <div className="col-md-6 p-5">
            <h2 className="text-primary text-center">Register</h2>
            <form
              className="d-grid gap-3 my-3"
              onSubmit={handleSubmit(onRegister)}
            >
              <div>
                <label htmlFor="name" className="form-label">
                  Name
                </label>
                <input
                  id="name"
                  type="text"
                  className="form-control"
                  placeholder="Eshmatjon"
                  {...register("name")}
                />
              </div>
              <div>
                <label htmlFor="username" className="form-label">
                  Username
                </label>
                <input
                  id="username"
                  type="text"
                  className="form-control"
                  placeholder="eshmatjon123"
                  {...register("username")}
                />
              </div>
              <div>
                <label htmlFor="password" className="form-label">
                  Password
                </label>
                <input
                  id="password"
                  type="password"
                  className="form-control"
                  placeholder="******"
                  {...register("password")}
                />
              </div>
              <button
                disabled={mutation.isPending}
                className="btn btn-primary w-100 rounded-pill"
              >
                Sign Up
              </button>
            </form>
            <p>
              Already have an account?{" "}
              <Link className="link-primary" to={"/login"}>
                Log In.
              </Link>
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Register;
