import { Link, Navigate, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useMutation } from "react-query";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { setUser } from "../store/Slices/user";
import { localTokenKey, reqTokenHederKey } from "../constants";
import { handleHTTPError } from "../Utils/api";

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { register, handleSubmit } = useForm();

  const mutation = useMutation({
    mutationFn: (user) => axios.post("/auth", user),
    onError: handleHTTPError,
    onSuccess: ({ data: { message, user, token } }) => {
      toast(message, { type: "success" });
      dispatch(setUser(user));
      axios.defaults.headers.common[reqTokenHederKey] = token;
      localStorage.setItem(localTokenKey, token);
      navigate("/main");
    },
  });

  const onLogin = async (values) => {
    await mutation.mutateAsync(values);
  };

  const token = localStorage.getItem(localTokenKey);

  if (token) return <Navigate to={"/main"} />;

  return (
    <section className="bg-secondary vh-100 d-flex align-items-center">
      <div className="container">
        <div className="row bg-white gx-3 rounded-4 overflow-hidden">
          <div className="col-md-6 text-center text-bg-dark p-5">
            <i className="fa-solid fa-blog fa-8x text-primary"></i>
            <p className="mt-5">Welcome back to</p>
            <h1 className="display-1">Shopping List</h1>
          </div>
          <div className="col-md-6 p-5">
            <h2 className="text-primary text-center">Sign In</h2>
            <form
              className="d-grid gap-3 my-3"
              onSubmit={handleSubmit(onLogin)}
            >
              <div>
                <label htmlFor="username" className="form-label">
                  Username
                </label>
                <input
                  id="username"
                  type="text"
                  className="form-control"
                  placeholder="eshmatjon123"
                  {...register("username")}
                />
              </div>
              <div>
                <label htmlFor="password" className="form-label">
                  Password
                </label>
                <input
                  id="password"
                  type="password"
                  className="form-control"
                  placeholder="******"
                  {...register("password")}
                />
              </div>
              <button
                disabled={mutation.isPending}
                className="btn btn-primary w-100 rounded-pill"
              >
                Sign In
              </button>
            </form>
            <p>
              No account yet?{" "}
              <Link className="link-primary" to={"/register"}>
                Create One.
              </Link>
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Login;
