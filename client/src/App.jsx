import { Navigate, Route, Routes, useNavigate } from "react-router-dom";
import Main from "./Layouts/Main";
import Group from "./Pages/Group";
import Home from "./Pages/Home";
import Login from "./Pages/Login";
import NotFound from "./Pages/NotFound";
import Register from "./Pages/Register";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { localTokenKey } from "./constants";
import axios from "axios";
import { setUser } from "./store/Slices/user";

function App() {
  const token = localStorage.getItem(localTokenKey);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (token)
      axios
        .get("/auth")
        .then(({ data }) => {
          dispatch(setUser(data));
        })
        .catch((error) => {
          console.log(error);
          localStorage.removeItem(localTokenKey);
          navigate("login");
        });
  }, [token, navigate, dispatch]);

  return (
    <>
      <Routes>
        <Route path="/" element={<Navigate to="/main" />} />
        <Route path="/main" element={<Main />}>
          <Route path="/main" element={<Home />} />
          <Route path="/main/groups/:groupId" element={<Group />} />
          <Route path="/main/*" element={<Navigate to="/main" />} />
        </Route>
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
}

export default App;
