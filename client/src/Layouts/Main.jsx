import { Navigate, Outlet } from "react-router-dom";
import { localTokenKey } from "../constants";
import Header from "../Partials/Header";
import Sidebar from "../Partials/Sidebar";

const Main = () => {
  const token = localStorage.getItem(localTokenKey);

  if (!token) return <Navigate to="/login" />;

  return (
    <div id="main-layout" className="bg-light">
      <Header />
      <Sidebar />
      <main className="p-3 pb-0 overflow-hidden rounded-top-5 d-flex flex-column">
        <Outlet />
      </main>
    </div>
  );
};

export default Main;
